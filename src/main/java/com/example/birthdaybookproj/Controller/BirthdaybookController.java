package com.example.birthdaybookproj.Controller;


import com.example.birthdaybookproj.DTO.BirthdaybookDTO;
import com.example.birthdaybookproj.Marshaller.BirthdaybookMarshaller;
import com.example.birthdaybookproj.Model.Birthdaybook;
import com.example.birthdaybookproj.Service.BirthdaybookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/birthdaybook")
@Component
@CrossOrigin
public class BirthdaybookController {
    String format = "yyyy-MM-dd";

    @Autowired
    private BirthdaybookService birthdaybookService;

    @Autowired
    private BirthdaybookMarshaller birthdaybookMarshaller;

    @PostMapping(consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public BirthdaybookDTO create(@RequestBody BirthdaybookDTO entry) throws ParseException {

        System.out.println("create !!!!!!!! ");

        Birthdaybook toSave = birthdaybookMarshaller.convertToModel(entry);

        Birthdaybook afterSave = birthdaybookService.create(toSave);

        return birthdaybookMarshaller.convertToDTO(afterSave);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public BirthdaybookDTO findById(@PathVariable("id") Long id) {
        System.out.println("find triggered !!!!!!! ");
        Birthdaybook entry = birthdaybookService.findById(id);


        return birthdaybookMarshaller.convertToDTO(entry);
    }

    @DeleteMapping(value = "/{id}")

    public void deleteById(@PathVariable("id") Long id) {
        System.out.println("delete triggered !!!!!!!!!! " + id);
        birthdaybookService.deleteById(id);
    }


    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)

    public BirthdaybookDTO update(@PathVariable("id") Long id,
                               @RequestBody BirthdaybookDTO entry) throws ParseException {
        System.out.println("update !!!!!!!!!!!! ");
        Birthdaybook newEntry  = birthdaybookMarshaller.convertToModel(entry);
        newEntry = birthdaybookService.update(newEntry);


        return birthdaybookMarshaller.convertToDTO(newEntry);
    }

    @GetMapping(value = "/all", produces = "application/json")
    public List<BirthdaybookDTO> findAll() {
        System.out.println("find all !!!!!!!! ");
        List<Birthdaybook> allbirths = new ArrayList<>();
        allbirths = birthdaybookService.findAll();
        List<BirthdaybookDTO> dtoList = new ArrayList<>();

        for(Birthdaybook eachbook : allbirths){
            dtoList.add(birthdaybookMarshaller.convertToDTO(eachbook));
        }


        return dtoList;
    }

    @GetMapping(value = "/searchdates/{startDate}/{endDate}", produces = "application/json")
    public List<BirthdaybookDTO> findByDates(@PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate) throws ParseException {
        System.out.println("find by date !!!!!!!!");
        Calendar calendar = Calendar.getInstance();
        DateFormat formatter = new SimpleDateFormat(format);
        Date startDateTime = formatter.parse(startDate);
        Date endDateTime = formatter.parse(endDate);
        calendar.setTime(startDateTime);
        long start = calendar.getTimeInMillis();
        calendar.setTime(endDateTime);
        long end = calendar.getTimeInMillis();

        List<Birthdaybook> birthList = birthdaybookService.findBetweenDate(start, end);
        List<BirthdaybookDTO> dtoList = new ArrayList<>();
        for(Birthdaybook eachbook : birthList){
            dtoList.add(birthdaybookMarshaller.convertToDTO(eachbook));
        }

        return dtoList;
    }




}
