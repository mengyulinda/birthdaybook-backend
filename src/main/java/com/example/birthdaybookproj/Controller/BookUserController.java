package com.example.birthdaybookproj.Controller;




import com.example.birthdaybookproj.DTO.BirthdaybookUserDTO;
import com.example.birthdaybookproj.Model.BirthdaybookUser;
import com.example.birthdaybookproj.Service.BirthdaybookUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/birthdaybookuser")
@Component
@CrossOrigin
public class BookUserController {
    @Autowired
    private BirthdaybookUserService birthdaybookuserService;

    @PostMapping(value = "/register",consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public BirthdaybookUser register(@RequestBody BirthdaybookUser entry){
        return birthdaybookuserService.create(entry);
    }

    @PostMapping(value = "/login",consumes = "application/json", produces = "application/json")
    public BirthdaybookUser login(@RequestBody BirthdaybookUserDTO bbudto) {

        BirthdaybookUser entry = birthdaybookuserService.findByUsernameAndPassword(bbudto.getUsername(), bbudto.getPassword());
        System.out.println(entry);
        return entry;
    }

}
