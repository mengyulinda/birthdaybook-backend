package com.example.birthdaybookproj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BirthdaybookprojApplication {

    public static void main(String[] args) {
        SpringApplication.run(BirthdaybookprojApplication.class, args);
    }

}
