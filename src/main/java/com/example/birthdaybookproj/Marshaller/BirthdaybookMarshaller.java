package com.example.birthdaybookproj.Marshaller;

import com.example.birthdaybookproj.DTO.BirthdaybookDTO;
import com.example.birthdaybookproj.Model.Birthdaybook;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class BirthdaybookMarshaller {

    String format = "yyyy-MM-dd";

    public Birthdaybook convertToModel(BirthdaybookDTO dto) throws ParseException {
        Birthdaybook b = new Birthdaybook();
        b.setId(dto.getId());
        b.setFullName(dto.getFullName());

        Calendar calendar = Calendar.getInstance();
        DateFormat formatter = new SimpleDateFormat(format);
        Date date = formatter.parse(dto.getBirthdayDateTime());
        calendar.setTime(date);

        b.setBirthdayDateTime(calendar.getTimeInMillis());
        return b;
    }

    public BirthdaybookDTO convertToDTO(Birthdaybook bb){
        BirthdaybookDTO dto = new BirthdaybookDTO();
        dto.setId(bb.getId());
        dto.setFullName(bb.getFullName());

        DateFormat formatter = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(bb.getBirthdayDateTime());
        dto.setBirthdayDateTime(formatter.format(calendar.getTime()));

        return dto;
    }
}
