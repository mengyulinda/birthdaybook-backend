package com.example.birthdaybookproj.Service.ServiceImpl;

import com.example.birthdaybookproj.Model.BirthdaybookUser;
import com.example.birthdaybookproj.Repository.BirthdaybookUserRepository;
import com.example.birthdaybookproj.Service.BirthdaybookUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;




@Component
public class BirthdaybookUserServiceImpl implements BirthdaybookUserService {

    @Autowired
    private BirthdaybookUserRepository birthdaybookuserRepository;

    @Override
    public BirthdaybookUser create(BirthdaybookUser bbuser) {
        return birthdaybookuserRepository.save(bbuser);
    }

    @Override
    public BirthdaybookUser findByUsernameAndPassword(String username, String password) {
        System.out.println(username);
        System.out.println(password);
        return birthdaybookuserRepository.findByUsernameAndPassword(username, password);

    }
}
