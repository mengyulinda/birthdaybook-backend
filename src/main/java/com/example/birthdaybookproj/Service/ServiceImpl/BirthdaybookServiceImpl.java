package com.example.birthdaybookproj.Service.ServiceImpl;

import com.example.birthdaybookproj.Model.Birthdaybook;
import com.example.birthdaybookproj.Repository.BirthdaybookRepository;
import com.example.birthdaybookproj.Service.BirthdaybookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class BirthdaybookServiceImpl implements BirthdaybookService {
    @Autowired
    private BirthdaybookRepository birthdaybookRepository;

    @Override
    public Birthdaybook create(Birthdaybook bb) {
        return birthdaybookRepository.save(bb);
    }

    @Override
    public Birthdaybook findById(Long id) {

        return birthdaybookRepository.findById(id).get();
    }

    @Override
    public void deleteById(Long id) {

        birthdaybookRepository.deleteById(id);
    }

    @Override
    public Birthdaybook update(Birthdaybook bb) {

        return birthdaybookRepository.save(bb);
    }

    @Override
    public List<Birthdaybook> findAll() {

        return birthdaybookRepository.findAll();
    }

    @Override
    public List<Birthdaybook> findBetweenDate(Long startDate, Long endDate) {
        return birthdaybookRepository.findAllByBirthdayDateTimeBetween(startDate, endDate);
    }
}