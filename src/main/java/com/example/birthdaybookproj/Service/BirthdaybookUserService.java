package com.example.birthdaybookproj.Service;

import com.example.birthdaybookproj.Model.BirthdaybookUser;


public interface BirthdaybookUserService {
    public BirthdaybookUser create(BirthdaybookUser bbuser);
    public BirthdaybookUser findByUsernameAndPassword(String username, String password);
}
