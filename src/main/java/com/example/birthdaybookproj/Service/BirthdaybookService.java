package com.example.birthdaybookproj.Service;

import com.example.birthdaybookproj.Model.Birthdaybook;

import java.util.List;

public interface BirthdaybookService {
    public Birthdaybook create(Birthdaybook bb);
    public Birthdaybook findById(Long id);
    public void deleteById(Long id);
    public Birthdaybook update(Birthdaybook bb);
    public List<Birthdaybook> findAll();
    public List<Birthdaybook> findBetweenDate(Long startDate, Long endDate);

}