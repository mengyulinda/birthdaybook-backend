package com.example.birthdaybookproj.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;
@Entity
public class Birthdaybook {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String fullName;
    private Long birthdayDateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Long getBirthdayDateTime() {
        return birthdayDateTime;
    }

    public void setBirthdayDateTime(Long birthdayDateTime) {
        this.birthdayDateTime = birthdayDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Birthdaybook that = (Birthdaybook) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(birthdayDateTime, that.birthdayDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, birthdayDateTime);
    }

    @Override
    public String toString() {
        return "Birthdaybook{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", birthdayDateTime=" + birthdayDateTime +
                '}';
    }
}
