package com.example.birthdaybookproj.Repository;
import com.example.birthdaybookproj.Model.BirthdaybookUser;
import org.springframework.data.repository.CrudRepository;

public interface BirthdaybookUserRepository extends CrudRepository<BirthdaybookUser,Long> {

    BirthdaybookUser findByUsernameAndPassword(String username, String password);
}
