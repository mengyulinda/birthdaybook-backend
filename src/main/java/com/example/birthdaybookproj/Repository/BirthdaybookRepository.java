package com.example.birthdaybookproj.Repository;

import com.example.birthdaybookproj.Model.Birthdaybook;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public interface BirthdaybookRepository extends CrudRepository<Birthdaybook,Long> {
    List<Birthdaybook> findAll();
    List<Birthdaybook> findAllByBirthdayDateTimeBetween(long startTime, long endTime);

}
