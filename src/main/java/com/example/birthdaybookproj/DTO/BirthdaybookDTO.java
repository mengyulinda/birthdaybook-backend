package com.example.birthdaybookproj.DTO;

import java.util.Date;
import java.util.Objects;

public class BirthdaybookDTO {
    private Long id;
    private String fullName;
    private String birthdayDateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBirthdayDateTime() {
        return birthdayDateTime;
    }

    public void setBirthdayDateTime(String birthdayDateTime) {
        this.birthdayDateTime = birthdayDateTime;
    }
}
